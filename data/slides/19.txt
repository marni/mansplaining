   ____  _____  ______ _   _    _____  ____  _    _ _____   _____ ______ 
  / __ \|  __ \|  ____| \ | |  / ____|/ __ \| |  | |  __ \ / ____|  ____|
 | |  | | |__) | |__  |  \| | | (___ | |  | | |  | | |__) | |    | |__   
 | |  | |  ___/|  __| | . ` |  \___ \| |  | | |  | |  _  /| |    |  __|  
 | |__| | |    | |____| |\  |  ____) | |__| | |__| | | \ \| |____| |____ 
  \____/|_|    |______|_| \_| |_____/ \____/ \____/|_|  \_\\_____|______|
                                                                         
    
                        /\   l^^^^^^^^^^^^^^^l  /\
                       /__\ _l O  O  O  O  O l_/__\
                       l   l l               l     l
                       l   l l               l     l
        [^^^]          l  [^^^]             [^^^]  l          [^^^]
        [ o ]-------------[ o ]-------------[ o ]-------------[ o ]
        [   ]             [   ]     ___     [   ]             [   ]
        [   ]             [   ]    /   \    [   ]             [   ]
        [   ]             [   ]   |     |   [   ]   o         [   ]
        [   ]             [   ]   |     |   [   ]~\_|_        [   ]
________[___]_____________[___]___|_____|___[___]_/\_/\_______[___]__


  [ o - - - o - - - - - - - - - o - - - - x - - - - - - o ] 
    |       |                   |         |             |
    v       v                   v         v             v
    Intro   Semantics           Syntax    Open Source   End!
